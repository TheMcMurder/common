import 'systemjs/dist/system.js'
import 'systemjs/dist/extras/amd.js'
import 'systemjs/dist/extras/named-exports.js'
import 'systemjs/dist/extras/use-default.js'
import 'systemjs/dist/extras/named-register.js'
import * as rxjs from 'rxjs'
import * as operators from 'rxjs/operators'

window.SystemJS = window.System

const deps = ['rxjs', 'rxjs/operators']
const systemResolve = System.resolve
System.resolve = function (name, parent) {
  if (deps.includes(name)) {
    return name
  }
  return systemResolve.call(this, name, parent)
}

System.set('rxjs', rxjs)
System.set('rxjs/operators', operators)
